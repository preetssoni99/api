const rcontroller = require('../models/video');
const routes = [
    {
        method: 'POST',
        url: '/api/post',
        handler: rcontroller.putVideo
    },
    {
        method: 'GET',
        url: '/api/getall',
        handler: rcontroller.getAllVideos
    },
    {
        method: 'GET',
        url: '/api/get/:title',
        handler: rcontroller.getVideo
    },
    {
        method: 'PUT',
        url: '/api/put',
        handler: rcontroller.updateVideoTitle
    },
   
]
module.exports = routes