const AWS = require("aws-sdk");

AWS.config.update({
  region: "us-west-2" ,// replace with your region in AWS account
  endpoint: "http://localhost:8000"
});

const DynamoDB = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();


exports.createTable = async (req, reply) => {
  const params = {
    TableName: "Videos",
    KeySchema: [{ AttributeName: "title", KeyType: "HASH" }],
    AttributeDefinitions: [{ AttributeName: "title", AttributeType: "S" }],
    ProvisionedThroughput: {
      ReadCapacityUnits: 10,
      WriteCapacityUnits: 10
    }
  };

  DynamoDB.createTable(params, function(err, data) {
    if (err) {
      console.error("Unable to create table", err);
    } else {
      console.log("Created table", data);
    }
  });
  
}

exports.putVideo = async (req, reply) => {
  const params = {
    TableName: "Videos",
    Item: {
      title: { S: req.body.title },
      desc: { S: req.body.desc }
    }
  };

  DynamoDB.putItem(params, function(err) {
    if (err) {
      console.error("Unable to add video", err);
    } else {
      console.log(`Added `);
    }
  });
  return {hello:"added a Video"}
}

exports.getAllVideos = async (req, reply) => {
  const params = {
    TableName: "Videos"
  };

  DynamoDB.scan(params, function(err, data) {
    if (err) {
      console.error("Unable to find Videos", err);
    } else {
      console.log(`Found Videos`);
      console.log(data.Items);
    }
  });
   reply.send({hello:"got_all_Videos"})
}

exports.getVideo = async (req, reply) => {
  const title = req.query.title
  console.log("title is ",title)
  const params = {
    TableName: "Videos",
    Key: {
      title: { S: title }
    }
  };

  DynamoDB.getItem(params, function(err, data) {
    if (err) {
      console.error("Unable to find Video", err);
    } else {
      console.log("Found Video", data.Item);
    }
  });
  reply.send({hello:"got_specific_Video"})
}

exports.updateVideoTitle = async (req, reply) => {
  var params = {
    TableName:"Videos",
    Key:{
        "title": req.body.title
    },
    UpdateExpression: "set description=:d",
    ExpressionAttributeValues:{
        ":d":req.body.new_title
    },
    ReturnValues:"UPDATED_NEW"
  };
  
  console.log("Updating the item...");
  docClient.update(params, function(err, data) {
    if (err) {
        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log('A branch2 edit')
        console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
  reply.send({hello:"updated_specific_Video"})
}



