const fastify = require('fastify')
const routes = require("./routes/routes")
const mdb=require('./models/video')

const app = fastify({
  logger: true
})


routes.forEach((route, index) => {
  app.route(route)
})

//mdb.createTable()   //instantiating table

// Declare a route
app.get('/', async (request, reply) => {
  return { hello: 'route works' }
})

const start = async () => {
  try {
    await app.listen(3000)
    app.log.info(`server listening on 3000`)
  } catch (err) {
    app.log.error(err)
    process.exit(1)
  }
}
start();